package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Cart extends BasePage {
    public Cart(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//*[@class=\"le-button ajax_add_to_cart_button\"]")
    public WebElement buyGoodsButton;

    @FindBy(xpath = "//*[@class=\"continue btn btn-default button button-medium\"]")
    public WebElement continueShoppingButton;

    @FindBy(xpath = "//*[@class=\"cart-items-count count ajax_cart_quantity\"][text()='1']")
    public WebElement goodsInTheCart;

    @FindBy(xpath = "//*[@class=\"logo\"]")
    public WebElement refreshPageButton;

    @FindBy(xpath = "//*[@title=\"Корзина\"][@alt=\"Корзина\"]")
    public WebElement theCartButton;

    @FindBy(xpath = "//*[@class=\"ajax_cart_block_remove_link\"]")
    public WebElement removeItemFromCartButton;

    @FindBy(xpath = "//*[@class=\"cart-items-count count ajax_cart_quantity\"][text()='0']")
    public WebElement theCartEmpty;

    @Step
    public Cart buyGoods(){
        buyGoodsButton.click();
        return new Cart(driver);
    }

    @Step
    public Cart continueShopping(){
        continueShoppingButton.click();
        return new Cart(driver);
    }

    @Step
    public Cart refreshPage(){
        refreshPageButton.click();
        return new Cart(driver);
    }

    @Step
    public Cart enterCart(){
        theCartButton.click();
        return new Cart(driver);
    }

    @Step
    public Cart removeItemFromCart(){
        removeItemFromCartButton.click();
        return new Cart(driver);
}
}
