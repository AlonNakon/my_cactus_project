package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LetterToTheDirector extends BasePage {


    public LetterToTheDirector(WebDriver webDriver) {
        super(webDriver);
        this.driver = webDriver;
    }


    @FindBy(xpath = "//*[@class=\"btn btn--border btn--director open-popup-link\"]")
    public WebElement writeToDirectorButton;

    @FindBy(xpath = "//*[@name=\"contact_name\"][@class=\"form-control form-control--color\"]")
    public WebElement fieldContactName;

    @FindBy(xpath = "//*[@class=\"form-control form-control--color\"][@name=\"contact_email\"]")
    public WebElement fieldContactEmail;

    @FindBy(xpath = "//*[@name=\"contact_message\"]")
    public WebElement fieldWriting;

    @FindBy(xpath = "//*[@class=\"btn btn--default director-submit\"]")
    public WebElement sendEmailButton;

    @FindBy(xpath = "//*[@class=\"request alert_success\"]")
    public WebElement emailSent;

    @FindBy(xpath = "//*[@class=\"small alert_error\"]")
    public WebElement fillInRequiredFields;

    @Step
    public LetterToTheDirector writeToDirector(){
        writeToDirectorButton.click();
        return new LetterToTheDirector(driver);
    }

    @Step
    public LetterToTheDirector contactName(String youName){
        fieldContactName.click();
        fieldContactName.sendKeys(youName);
        return new LetterToTheDirector(driver);
    }

    @Step
    public LetterToTheDirector contactEmail(String youEmail){
        fieldContactEmail.sendKeys(youEmail);
        return new LetterToTheDirector(driver);
    }

    @Step
    public LetterToTheDirector letterContent(String content){
        fieldWriting.sendKeys(content);
        return new LetterToTheDirector(driver);
    }

    @Step
    public LetterToTheDirector sendEmail(){
        sendEmailButton.click();
        return new LetterToTheDirector(driver);
    }

}
