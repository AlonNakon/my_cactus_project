package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class PersonalArae extends BasePage{

    public PersonalArae(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver,this);
    }



    @FindBy(xpath = "//*[@id=\"email\"]")
    public WebElement emailField;

    @FindBy(xpath = "//*[@id=\"passwd\"]")
    public WebElement passwordField;

    @FindBy(xpath = "//*[@style=\"list-style:none;\"]")
    public WebElement authorisationError;

    @FindBy(xpath = "//*[@class=\"account\"]")
    public WebElement accountButton;

    @FindBy(xpath = "//*[@id=\"SubmitLogin\"]")
    public WebElement entrance;

    @FindBy(xpath = "//*[@class=\"page-title\"]")
    public WebElement personalAccountLogin;

    @FindBy(xpath = "//*[@id=\"email_create\"]")
    public WebElement registrationEmail;

    @FindBy(xpath = "//*[@id=\"SubmitCreate\"]/span")
    public WebElement registrationButton;

    @FindBy(xpath = "//*[@class=\"account_creation\"]")
    public WebElement personalData;

    @FindBy(xpath = "//*[@id=\"customer_firstname\"]")
    public WebElement personalFirstName;

    @FindBy(xpath = "//*[@id=\"customer_lastname\"]")
    public WebElement personalLastName;

    @FindBy(xpath = "//*[@id=\"phone_mobile\"]")
    public WebElement personalPhone;

    @FindBy(xpath = "//*[@id=\"passwd\"]")
    public WebElement personalPassword;

    @FindBy(xpath = "//*[@id=\"submitAccount\"]/span")
    public WebElement submitAccountButton;

    @FindBy(xpath = "//*[@class=\"alert alert-success\"]")
    public WebElement newAccountCreate;



    @Step
    public PersonalArae enterAccountButton (){
        accountButton.click();
        return new PersonalArae(driver);

    }

    @Step
    public PersonalArae inputEmail(String email){
        emailField.sendKeys(email);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae inputPassword(String password){
        passwordField.sendKeys(password);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae personalEntrance(){
        entrance.click();
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae registrationMail(String newMail){
        registrationEmail.sendKeys(newMail);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae toRegister (){
        registrationButton.click();
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae inputFirstName(String youFirstName){
        personalFirstName.sendKeys(youFirstName);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae inputLastName(String youLastName){
        personalLastName.sendKeys(youLastName);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae inputPhone(String youPhone){
        personalPhone.click();
        personalPhone.sendKeys(youPhone);
        return new PersonalArae(driver);

    }

    @Step
    public PersonalArae inputPossword(String youPossword){
        personalPassword.sendKeys(youPossword);
        return new PersonalArae(driver);
    }

    @Step
    public PersonalArae submitAccount(){
        submitAccountButton.click();
        return new PersonalArae(driver);
    }


}
