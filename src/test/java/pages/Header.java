package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;

public class Header extends BasePage {

    public Header (WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//*[@class=\"heading-counter\"]")
    public WebElement searchResultFindText;

    @FindBy(xpath = "//*[contains(text(),'Ничего не найдено')]")
    public WebElement searchFailed;

    @FindBy (xpath = "//*[@id=\"search_query_block\"]")
    public WebElement searchField;

    @FindBy(xpath = "//*[@id=\"search_button\"]")
    public WebElement searchButton;

    @FindBy(xpath = "//*[@class=\"le-button ajax_add_to_cart_button\"]")
    public WebElement buyGoodsButton;

    @FindBy(xpath = "//*[@class=\"continue btn btn-default button button-medium\"]")
    public WebElement continueShoppingButton;

    @FindBy(xpath = "//*[@class=\"cart-items-count count ajax_cart_quantity\"][text()='1']")
    public WebElement goodsInTheCart;

    @FindBy(xpath = "//*[@class=\"logo\"]")
    public WebElement refreshPageButton;

    @FindBy(xpath = "//*[@title=\"Корзина\"][@alt=\"Корзина\"]")
    public WebElement theCartButton;

    @FindBy(xpath = "//*[@class=\"ajax_cart_block_remove_link\"]")
    public WebElement removeItemFromCartButton;

    @FindBy(xpath = "//*[@class=\"cart-items-count count ajax_cart_quantity\"][text()='0']")
    public WebElement theCartEmpty;

    @FindBy(xpath = "//*[@class=\"subcategory-name\"][@title=\"Apple iPad\"]")
    public WebElement selectCategoryIPadButton;

    @FindBy(xpath = "//*[@data-id-product=\"7124\"][@title=\"Сравнить\"]")
    public WebElement firstProductCompareButton;

    @FindBy(xpath = "//*[@data-id-product=\"8598\"][@title=\"Сравнить\"]")
    public WebElement secondProductCompareButton;

    @FindBy(xpath = "//*[@class=\"fancybox-item fancybox-close\"]")
    public WebElement continueComparisonButton;

    @FindBy(xpath = "//*[@class=\"comparison_link\"]")
    public WebElement goToTheComparisonPageButton;

    @FindBy(xpath = "//*[@class=\"fa fa-times-circle\"]")
    public WebElement removeCompareProductButton;

    @FindBy(xpath = "//*[@class=\"compare-header\"]")
    public WebElement compareButton;

    @FindBy(xpath = "//*[@class=\"lead-title text-center cart-empty\"]")
    public WebElement comparePageIsEmpty;

    @FindBy(xpath = "//*[@class=\"lead-title text-center cart-empty\"]")
    public WebElement comparisonPage;

    @FindBy(xpath = "//*[@class=\"main-img\"][@alt=\"instagram\"]")
    public WebElement selectInstagramButton;

    @FindBy(xpath = "//*[@class=\"glyphsSpriteMobile_nav_type_logo u-__7\"]")
    public WebElement instagramPage;

    @FindBy(xpath = "//*[@class=\"main-img\"][@alt=\"facebook\"]")
    public WebElement selectFacebookButton;

    @FindBy(xpath = "//*[@class=\"fb_logo img sp_X-9DNUJiWfS sx_6b1cd1\"]")
    public WebElement facebookPage;

    @FindBy(xpath = "//*[@class=\"main-img\"][@alt=\"google-plus\"]")
    public WebElement selectGooglePlusButton;

    @FindBy(xpath = "//*[@id=\"logo\"]/img")
    public WebElement googlePage;

    @FindBy(xpath = "//*[@class=\"main-img\"][@alt=\"telegram\"]")
    public WebElement selectTelegramButton;

    @FindBy(xpath = "//*[@class=\"tgme_logo\"]")
    public WebElement telegramPage;

    @FindBy(xpath = "//*[@class=\"product_img_link\"]")
    public WebElement openFirstSearchResult;

    @FindBy(xpath = "//*[@id=\"image-block\"]")
    public WebElement clickPhotoProduct;

    @FindBy(xpath = "//*[@class=\"fancybox-image\"]")
    public WebElement zoomProductIsOpen;

    @FindBy(xpath = "//*[@class=\"products_color \"][@title=\"Apple iPhone XR Dual Sim 64 Гб (Коралловый)\"]")
    public WebElement chooseCoralColorOfThePhoneButton;

    @FindBy(xpath = "//*[@id=\"bigpic\"][@alt=\"Apple iPhone XR 64 Гб (Коралловый)\"]")
    public WebElement photoPhoneCoral;

    @FindBy(xpath = "//*[@id=\"toplink02\"]")
    public WebElement quickMenuButton;

    @FindBy(xpath = "//*[@id=\"title\"]")
    public WebElement goToThePageThroughTheQuickMenu;

    @FindBy(xpath = "//*[@class=\"btn btn--border btn--director open-popup-link\"]")
    public WebElement writeToDirectorButton;

    @FindBy(xpath = "//*[@name=\"contact_name\"][@class=\"form-control form-control--color\"]")
    public WebElement fieldContactName;

    @FindBy(xpath = "//*[@class=\"form-control form-control--color\"][@name=\"contact_email\"]")
    public WebElement fieldContactEmail;

    @FindBy(xpath = "//*[@name=\"contact_message\"]")
    public WebElement fieldWriting;

    @FindBy(xpath = "//*[@class=\"btn btn--default director-submit\"]")
    public WebElement sendEmailButton;

    @FindBy(xpath = "//*[@class=\"request alert_success\"]")
    public WebElement emailSent;

    @FindBy(xpath = "//*[@class=\"small alert_error\"]")
    public WebElement fillInRequiredFields;


    @Step
    public Header enterInputText(String inputText){
        searchField.sendKeys(inputText);
        return this;
    }

    @Step
    public Header performSearchByEnter(){
        searchField.sendKeys(Keys.ENTER);
        return new Header(driver);
    }

    @Step
    public Header performSearchByClickButton(){
        searchButton.click();
        return new Header(driver);
    }

    @Step
    public Header buyGoods(){
        buyGoodsButton.click();
        return new Header(driver);
    }

    @Step
    public Header continueShopping(){
        continueShoppingButton.click();
        return new Header(driver);
    }

    @Step
    public Header refreshPage(){
        refreshPageButton.click();
        return new Header(driver);
    }

    @Step
    public Header enterCart(){
        theCartButton.click();
        return new Header(driver);
    }

    @Step
    public Header removeItemFromCart(){
        removeItemFromCartButton.click();
        return new Header(driver);
    }

    @Step
    public Header selectCategoryIPad(){
        selectCategoryIPadButton.click();
        return new Header(driver);
    }

    @Step
    public Header firstProductCompare(){
        firstProductCompareButton.click();
        return new Header(driver);
    }

    @Step
    public Header secondProductCompare(){
        secondProductCompareButton.click();
        return new Header(driver);
    }

    @Step
    public Header continueComparison(){
        continueComparisonButton.click();
        return new Header(driver);
    }

    @Step
    public Header goToTheComparisonPage(){
        goToTheComparisonPageButton.click();
        return new Header(driver);
    }

    @Step
    public Header deleteCompareProduct(){
        removeCompareProductButton.click();
        return new Header(driver);
    }

    @Step
    public Header openComparePage(){
        compareButton.click();
        return new Header(driver);
    }

    @Step
    public Header selectInstagram(){
        selectInstagramButton.click();
        return new Header(driver);
    }

    @Step
    public Header openSecondPage(){
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        return new Header(driver);
    }

    @Step
    public Header selectFacebook(){
        selectFacebookButton.click();
        return new Header(driver);
    }

    @Step
    public Header selectGooglePlus(){
        selectGooglePlusButton.click();
        return new Header(driver);
    }

    @Step
    public Header selectTelegram(){
        selectTelegramButton.click();
        return new Header(driver);
    }

    @Step
    public Header clickFirstSearchResult(){
        openFirstSearchResult.click();
        return new Header(driver);
    }

    @Step
    public Header openPhotoProduct(){
        clickPhotoProduct.click();
        return new Header(driver);

    }

    @Step
    public Header clickChooseCoralColorOfThePhone(){
        chooseCoralColorOfThePhoneButton.click();
        return new Header(driver);
    }

    @Step
    public Header clickQuickMenu(){
        quickMenuButton.click();
        return new Header(driver);
    }

    @Step
    public Header writeToDirector(){
        writeToDirectorButton.click();
        return new Header(driver);
    }

    @Step
    public Header contactName(String youName){
        fieldContactName.click();
        fieldContactName.sendKeys(youName);
        return new Header(driver);
    }

    @Step
    public Header contactEmail(String youEmail){
        fieldContactEmail.sendKeys(youEmail);
        return new Header(driver);
    }

    @Step
    public Header letterContent(String content){
        fieldWriting.sendKeys(content);
        return new Header(driver);
    }

    @Step
    public Header sendEmail(){
        sendEmailButton.click();
        return new Header(driver);
    }


}
