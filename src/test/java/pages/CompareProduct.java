package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompareProduct extends BasePage {
    public CompareProduct(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy (xpath = "//*[@id=\"search_query_block\"]")
    public WebElement searchField;


    @FindBy(xpath = "//*[@class=\"subcategory-name\"][@title=\"Apple iPad\"]")
    public WebElement selectCategoryIPadButton;

    @FindBy(xpath = "//*[@data-id-product=\"7124\"][@title=\"Сравнить\"]")
    public WebElement firstProductCompareButton;

    @FindBy(xpath = "//*[@data-id-product=\"8598\"][@title=\"Сравнить\"]")
    public WebElement secondProductCompareButton;

    @FindBy(xpath = "//*[@class=\"fancybox-item fancybox-close\"]")
    public WebElement continueComparisonButton;

    @FindBy(xpath = "//*[@class=\"comparison_link\"]")
    public WebElement goToTheComparisonPageButton;

    @FindBy(xpath = "//*[@class=\"fa fa-times-circle\"]")
    public WebElement removeCompareProductButton;

    @FindBy(xpath = "//*[@class=\"compare-header\"]")
    public WebElement compareButton;

    @FindBy(xpath = "//*[@class=\"lead-title text-center cart-empty\"]")
    public WebElement comparePageIsEmpty;

    @FindBy(xpath = "//*[@class=\"lead-title text-center cart-empty\"]")
    public WebElement comparisonPage;

    @FindBy(xpath = "//*[@class=\"logo\"]")
    public WebElement refreshPageButton;





    @Step
    public CompareProduct enterInputText(String inputText){
        searchField.sendKeys(inputText);
        return this;
    }

    @Step
    public CompareProduct performSearchByEnter(){
        searchField.sendKeys(Keys.ENTER);
        return new CompareProduct(driver);
    }


    @Step
    public CompareProduct selectCategoryIPad(){
        selectCategoryIPadButton.click();
        return new CompareProduct(driver);
    }

    @Step
    public CompareProduct firstProductCompare(){
        firstProductCompareButton.click();
        return new CompareProduct(driver);
    }

    @Step
    public CompareProduct secondProductCompare(){
        secondProductCompareButton.click();
        return new CompareProduct(driver);
    }

    @Step
    public CompareProduct continueComparison(){
        continueComparisonButton.click();
        return new CompareProduct(driver);
    }

    @Step
    public CompareProduct goToTheComparisonPage(){
        goToTheComparisonPageButton.click();
        return new CompareProduct(driver);
    }

    @Step
    public CompareProduct deleteCompareProduct(){
        removeCompareProductButton.click();
        return new CompareProduct(driver);
    }


    @Step
    public CompareProduct refreshPage(){
        refreshPageButton.click();
        return new CompareProduct(driver);
    }


    @Step
    public CompareProduct openComparePage(){
        compareButton.click();
        return new CompareProduct(driver);
    }

}
