package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductSelection extends BasePage {
    public ProductSelection(WebDriver webDriver) {
        super(webDriver);
        this.driver = webDriver;
    }

    @FindBy(xpath = "//*[@class=\"product_img_link\"]")
    public WebElement openFirstSearchResult;

    @FindBy(xpath = "//*[contains(text(),'Ничего не найдено')]")
    public WebElement searchFailed;

    @FindBy(xpath = "//*[@id=\"image-block\"]")
    public WebElement clickPhotoProduct;

    @FindBy(xpath = "//*[@class=\"fancybox-image\"]")
    public WebElement zoomProductIsOpen;

    @FindBy(xpath = "//*[@class=\"products_color \"][@title=\"Apple iPhone XR Dual Sim 64 Гб (Коралловый)\"]")
    public WebElement chooseCoralColorOfThePhoneButton;

    @FindBy(xpath = "//*[@id=\"bigpic\"][@alt=\"Apple iPhone XR 64 Гб (Коралловый)\"]")
    public WebElement photoPhoneCoral;

    @FindBy(xpath = "//*[@id=\"toplink02\"]")
    public WebElement quickMenuButton;

    @FindBy(xpath = "//*[@id=\"title\"]")
    public WebElement goToThePageThroughTheQuickMenu;

    @Step
    public ProductSelection enterInputText(String inputText){
        searchFailed.sendKeys(inputText);
        return this;
    }

    @Step
    public ProductSelection performSearchByEnter(){
        searchFailed.sendKeys(Keys.ENTER);
        return new ProductSelection(driver);
    }

    @Step
    public ProductSelection clickFirstSearchResult(){
        openFirstSearchResult.click();
        return new ProductSelection(driver);
    }

    @Step
    public ProductSelection openPhotoProduct(){
        clickPhotoProduct.click();
        return new ProductSelection(driver);

    }

    @Step
    public ProductSelection clickChooseCoralColorOfThePhone(){
        chooseCoralColorOfThePhoneButton.click();
        return new ProductSelection(driver);
    }

    @Step
    public ProductSelection clickQuickMenu(){
        quickMenuButton.click();
        return new ProductSelection(driver);
    }
}
