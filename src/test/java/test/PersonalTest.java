package test;

import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.testng.annotations.Test;
import pages.PersonalArae;

public class PersonalTest extends BaseTest {

    String email = "alenanakonechnay16@gmail.com";
    String password = "YWRJXU3f";
    String invaliEmail = "alenanakonechnay116@gmail.com";
    String invalidPassword = "123456";
    RandomString randomEmail = new RandomString();
    String phone = "0630994356";

    @Test
    public void enterYourProfile(){
        PersonalArae personalArae = new PersonalArae(driver).enterAccountButton();
        personalArae.inputEmail(email)
                .inputPassword(password)
                .entrance
                .isDisplayed();
        Assert.assertTrue(personalArae.personalEntrance().personalAccountLogin.isDisplayed());
    }

    @Test
    public void enterYourProfileWithInvalidPassword(){
        PersonalArae personalArae = new PersonalArae(driver).enterAccountButton();
        personalArae.inputEmail(email)
                .inputPassword(invalidPassword)
                .entrance
                .isDisplayed();
        Assert.assertTrue(personalArae.personalEntrance().authorisationError.isDisplayed());
    }

    @Test
    public void enterYourProfileWithInvalidEmail(){
        PersonalArae personalArae = new PersonalArae(driver).enterAccountButton();
        personalArae.inputEmail(invaliEmail)
                .inputPassword(password)
                .entrance
                .isDisplayed();
        Assert.assertTrue(personalArae.personalEntrance().authorisationError.isDisplayed());
    }

    @Test
    public void createNewAccount(){
        PersonalArae personalArae = new PersonalArae(driver).enterAccountButton();
        personalArae.registrationMail(randomEmail+"gmail.com").toRegister();
        personalArae.inputFirstName("Lady");
        personalArae.inputLastName("Gaga");
        personalArae.inputPhone(phone);
        personalArae.inputPossword(password);
        personalArae.submitAccount();
        Assert.assertTrue(personalArae.newAccountCreate.isDisplayed());
    }
}
