package test;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.Header;

public class CompareProductTest extends BaseTest {

    @Test
    public void compareProduct(){
        Header comparisonPage = new Header(driver)
                .enterInputText("Apple")
                .performSearchByEnter()
                .selectCategoryIPad()
                .firstProductCompare()
                .continueComparison()
                .secondProductCompare()
                .goToTheComparisonPage();
        Assert.assertTrue(comparisonPage.comparisonPage.isDisplayed());
    }

    @Test
    public void removeCompareProduct(){
        Header comparisonPage = new Header(driver)
                .enterInputText("Apple")
                .performSearchByEnter()
                .selectCategoryIPad()
                .firstProductCompare()
                .goToTheComparisonPage()
                .deleteCompareProduct()
                .refreshPage()
                .openComparePage();
        Assert.assertTrue(comparisonPage.comparePageIsEmpty.isDisplayed());
    }
}
