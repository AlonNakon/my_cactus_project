package test;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.Header;


public class HomeTest extends BaseTest {


    @Test
    public void findSearchResultByEnter(){
        Header searchResult = new Header(driver)
                .enterInputText("Apple")
                .performSearchByEnter();
        Assert.assertTrue(searchResult.searchResultFindText.isDisplayed());

    }

    @Test
    public void findSearchResultBySearchButton(){
        Header searchResult = new Header(driver)
                .enterInputText("Apple")
                .performSearchByClickButton();
        Assert.assertTrue(searchResult.searchResultFindText.isDisplayed());

    }

    @Test
    public void noSearchResult(){
        Header searchResult = new Header(driver)
                .enterInputText("Ajkfdvbdabv")
                .performSearchByEnter();
        Assert.assertTrue(searchResult.searchFailed.isDisplayed());
    }


/*    @Test
    public void choiceInstagramPage(){
        Header instPage = new Header(driver)
                .selectInstagram()
                .openSecondPage();
        Assert.assertTrue(instPage.instagramPage.isDisplayed());
    }

    @Test
  public void choiceFacebookPage(){
        Header fbPage = new Header(driver)
                .selectFacebook()
                .openSecondPage();
        Assert.assertTrue(fbPage.facebookPage.isDisplayed());
    }

    @Test
    public void choiceGooglePlusPage(){
        Header googPlusPage = new Header(driver)
                .selectGooglePlus()
                .openSecondPage();
        Assert.assertTrue(googPlusPage.googlePage.isDisplayed());
    }

    @Test
    public void choiceTelegramPage(){
        Header telegPage = new Header(driver)
                .selectTelegram()
                .openSecondPage();
        Assert.assertTrue(telegPage.telegramPage.isDisplayed());
    }*/


    @Test
    public void quickMenu(){
        Header qMenu = new Header(driver)
                .clickQuickMenu();
        Assert.assertTrue(qMenu.goToThePageThroughTheQuickMenu.isDisplayed());
    }


}
