package test;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.Header;


public class CartTest extends BaseTest{

    @Test
    public void addItemToCart(){
        Header addItemToCart = new Header(driver)
                .buyGoods()
                .continueShopping()
                .refreshPage();
        Assert.assertTrue(addItemToCart.goodsInTheCart.isDisplayed());
    }

    @Test
    public void removeFromCart(){
        Header removeItemToCart = new Header(driver)
                .buyGoods()
                .continueShopping()
                .refreshPage()
                .enterCart()
                .removeItemFromCart();
        Assert.assertTrue(removeItemToCart.theCartEmpty.isDisplayed());
    }
}
