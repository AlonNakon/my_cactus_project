package test;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.Header;


public class LetterToTheDirectorTest extends BaseTest {

    String email = "alenanakonechnay16@gmail.com";

    @Test
    public void sendingLetterToTheDirector(){
        Header sendingLetter = new Header(driver)
                .writeToDirector()
                .contactName("Lady Gaga")
                .contactEmail(email)
                .letterContent("  ")
                .sendEmail();
        Assert.assertTrue(sendingLetter.emailSent.isDisplayed());
    }

    @Test
    public void sendingLetterToTheDirectorWithoutName(){
        Header sendingLetterWithoutName = new Header(driver)
                .writeToDirector()
                .contactEmail(email)
                .letterContent("  ")
                .sendEmail();
        Assert.assertTrue(sendingLetterWithoutName.fillInRequiredFields.isDisplayed());
    }
}
