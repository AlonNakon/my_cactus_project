package test;

import org.junit.Assert;
import org.testng.annotations.Test;
import pages.Header;


public class ProductSelectionTest extends BaseTest {

    @Test
    public void zoomProduct(){
        Header zoomPrdt = new Header(driver)
                .enterInputText("Apple iPhone XR Dual Sim 64 Гб (Красный)")
                .performSearchByEnter()
                .clickFirstSearchResult()
                .openPhotoProduct();
        Assert.assertTrue(zoomPrdt.zoomProductIsOpen.isDisplayed());
    }

    @Test
    public void changeProductColor(){
        Header productColor = new Header(driver)
                .enterInputText("Apple iPhone XR Dual Sim 64 Гб (Красный)")
                .performSearchByEnter()
                .clickFirstSearchResult()
                .clickChooseCoralColorOfThePhone();
        Assert.assertTrue(productColor.photoPhoneCoral.isDisplayed());
    }
}
